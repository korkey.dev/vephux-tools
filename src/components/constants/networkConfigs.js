export const networks = [
    {
        id: 'pulsechain',
        chainId: '369',
        name: 'PulseChain',
        graphQLEndPoint: 'https://sub5.phatty.io/subgraphs/name/phux/pools-v3',
        url: 'https://phux.io/#/pulse',
    }
];