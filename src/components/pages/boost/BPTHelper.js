import React from "react";
import { Typography } from "@mui/material";
import { Box } from "@mui/system";

export default function BPTHelper() {

    return (
            <Box>
                <Typography>When PPT is locked for 1 full year</Typography>
                <Typography>Use the Token Estimator tab above for vePHUX configurations</Typography>
            </Box>
    )
}