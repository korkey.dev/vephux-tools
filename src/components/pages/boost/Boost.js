import React from "react";
import Header from "../../UI/Header";
import Grid from '@mui/material/Grid';
import { Box } from '@mui/material';
import BoostForm from "./BoostForm";
import VePHUXLogo from './../../../resources/vePHUX.png'
import Alert from '@mui/material/Alert';
//import BribeHelper from "./BribeHelper";

export default function Boost(props) {
    return (
        <div key={props.networkId} >
            <Grid item xs={12}>
                <Header>vePHUX Boost Calculator</Header>
                <Box >
                    <img src={VePHUXLogo} alt="vePHUX Logo" width="120" />
                </Box>
            </Grid>
            {props.networkId === 'pulsechain' ?
                <Grid item xs={12} justifyContent="center">
                    <BoostForm darkState={props.darkState} networkId={props.networkId}></BoostForm>
                </Grid>
                :
                <Box p={0.5} display="flex" flexDirection="row" justifyContent="center">
                <Alert severity="info">
                    Chain does not support vePHUX boosting!
                </Alert>
                </Box>
                
            }
            {/*<BribeHelper networkId={props.networkId}></BribeHelper> */}
        </div>
    );
}