import './App.css';
import Dashboard from './components/UI/Dashboard'
import {
  ApolloClient,
  InMemoryCache,
  ApolloProvider,
} from "@apollo/client";
import { RetryLink } from 'apollo-link-retry';
import { HttpLink } from '@apollo/client';

//Set up directional links
const directionalLink =
  new RetryLink().split(
  (operation) => operation.getContext().clientName === 'pulsechain',
  new HttpLink({ uri: "https://sub5.phatty.io/subgraphs/name/phux/pools-v3" }),
  );


//Instantiate Apollo client
const client = new ApolloClient({
  cache: new InMemoryCache(),
  link: directionalLink,
});

function App() {
  return (
    
    <div className="App">
      <ApolloProvider client={client}>
      <Dashboard/>
      </ApolloProvider>
    </div>
  );
}

export default App;
