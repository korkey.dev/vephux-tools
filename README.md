# Getting Started with PHUX Tools
## How to install
* Initialize repo locally:
`git remote add origin https://gitlab.com/korkey.dev/vephux-tools.git`
* Install Node.js
* Install npm
* In the project folder install needed dependencies: run `npm install`
* Run the project with `npm start`


## What this project wants to achieve
Balancer.tools is a community tooling site to better understand the Balancer ecosystem. It provides advanced tools like a vePHUX boosting calculator, impermanent loss calculator and much more.

## Current features

* vePHUX boost calculator
* Impermanent loss calculator
* Price impact calculator for swaps and investments
* Endpoint status checker

## Future features in review

* Metamask integration
* Impermanent loss calculation per address
* Incentives overview with APR ranges for new gauge system
* Fallback UI to withdraw funds from Balancer pools / contracts
* Helpers such as gauge vote allocation